#include "efm32lg990f256.h"
#include "stdbool.h"

#ifndef buttons_h
#define buttons_h

void button_init(void); 

bool button0_read(void);
bool button1_read(void);

#endif
