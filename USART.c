#include "efm32lg990f256.h"
#include "USART.h"
#include <string.h>

#define USART_PORT 2 //Use USART0 localization 5
#define USART_TX 0   //PTC0 
#define USART_RX 1   //PTC1
#define CLOCK_DIVIDER 14
#define USART_CLOCK_DIVIDER 621
#define USART_CLOCK_DIVIDER_MASK 6

void usart_init (void){
	
	CMU->CTRL |= (1 << CLOCK_DIVIDER);                              									// Set system clock to 24MHz
  CMU->OSCENCMD |= CMU_OSCENCMD_HFXOEN;                                
  while(! (CMU->STATUS & CMU_STATUS_HFXORDY) );                       
  CMU->CMD = CMU_CMD_HFCLKSEL_HFXO;  
	CMU->HFPERCLKEN0 |= CMU_HFPERCLKEN0_GPIO | CMU_HFPERCLKEN0_USART0;
	
	GPIO->P[USART_PORT].MODEL |= (1 << (4*USART_RX))|(4 << (4*USART_TX));  						// Set TX as output, RX as input
	GPIO->P[USART_PORT].DOUTSET |= (1 << USART_TX);  																	// Initialize TX high
	USART0->CLKDIV = (USART_CLOCK_DIVIDER << USART_CLOCK_DIVIDER_MASK);								// Set clkdiv to achive 9600 baud rate
	USART0->CMD = USART_CMD_CLEARRX|USART_CMD_CLEARTX |USART_CMD_TXEN|USART_CMD_RXEN; // Enable TX and RX, clear buffers
	USART0->IFC = _UART_IFC_MASK;																											//Clear all USART interrupt flags
	USART0->ROUTE = UART_ROUTE_LOCATION_LOC5| UART_ROUTE_RXPEN |UART_ROUTE_TXPEN;			// Use USART location 5, enable pins	
} 

void usart_writeChar (char c){
	
	while( !(USART0->STATUS & (USART_STATUS_TXBL)) ); // wait for TX buffer to empty
	USART0->TXDATA = c;
	while( !(USART0->STATUS & (USART_STATUS_TXBL)) ); // wait for TX buffer to empty
}

void usart_writeString (char* string){
	
	for(int i=0; i<strlen(string); i++){
			usart_writeChar(string[i]);
	}

}

char usart_readChar (void){
	
	while( !(USART0->STATUS & (USART_STATUS_RXDATAV)) ); // wait for TX buffer to empty
	return USART0->RXDATA; 

}
