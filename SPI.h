#include "efm32lg990f256.h"
#ifndef spi_h
#define spi_h

void SPI_init (void);
void SPI_putChar (uint8_t c);
uint8_t SPI_getChar (void);
uint16_t SPI_get2Char (void);
void CS_clr (void);
void CS_set (void);
void SPI_disable (void);
void SPI_enable(void);
#endif
