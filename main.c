#include "efm32lg990f256.h"
#include "em_chip.h"
#include "em_emu.h"
#include "BMP280.h"
#include "MCP9808.h"
#include "I2C.h"
#include "SPI.h"
#include "RTC.h"
#include "LEUART.h"
#include "bluetooth.h"

void RTC_IRQHandler(void) {

	RTC_stop();
	turnBluetoothOn();
	waitForBluetoothPaired();
	weather_station_run();
	turnBluetoothOff();
	RTC_clear_interrupts();
	RTC_start();
}

int main() {
	extern struct sensor BMP_sensor;
	extern struct sensor MPC_sensor;
	
	weather_station_init(leuart_readChar,leuart_writeChar,RTC_set_period);
	weather_station_add_sensor(&MPC_sensor);
	weather_station_add_sensor(&BMP_sensor);
	
	CHIP_Init();
	RTC_init();
	initiBluetooth();
	turnBluetoothOn();
	weather_station_run();
	turnBluetoothOff();
	RTC_start();

  while(1) {
		EMU_EnterEM2(true); 
	}
}
