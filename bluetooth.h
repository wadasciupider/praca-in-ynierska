#include <stdbool.h>
#include "efm32lg990f256.h"
#ifndef bluetooth_h
#define bluetooth_h

void waitForBluetoothPaired(void);
void initiBluetooth(void);
void turnBluetoothOn(void);
void turnBluetoothOff(void);

#endif
