#include "efm32lg990f256.h"
#include "RTC.h"

void RTC_init(){
	CMU->OSCENCMD |= CMU_OSCENCMD_LFRCOEN;
	CMU->LFACLKEN0 |= CMU_LFACLKEN0_RTC;
	CMU->LFAPRESC0 |= CMU_LFAPRESC0_RTC_DIV256;
	CMU->HFCORECLKEN0 |= CMU_HFCORECLKEN0_LE;
	RTC->CTRL |= RTC_CTRL_COMP0TOP;
	RTC->IEN = RTC_IEN_COMP0; // disable other interrupts
	RTC->IFC = RTC_IFC_COMP0|RTC_IFC_COMP1|RTC_IFC_OF;
	NVIC_ClearPendingIRQ(RTC_IRQn);
	NVIC_EnableIRQ(RTC_IRQn);
}
void RTC_clear_interrupts(void){
	RTC->IFC = RTC_IFC_COMP0;
	NVIC_ClearPendingIRQ(RTC_IRQn);
}
void RTC_start(void){
	RTC->CTRL |= RTC_CTRL_EN;

}
void RTC_stop(void){
	RTC->CTRL &= ~RTC_CTRL_EN;
}
void RTC_set_period(uint32_t periodInSeconds){

	RTC->COMP0 = 128*periodInSeconds;

}
