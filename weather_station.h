#include <stdint.h>
#ifndef weather_station_h
#define weather_station_h

#define START_TRANSMISSION_BYTE 100
#define MAXIMUM_NUMBER_OF_SENSORS 8

struct sensor 
{
   void (*initialize)(void);
   void (*deinitialize)(void);
	 void (*start)(void);
   void (*stop)(void);
   uint32_t (*get_data)(void);
};

void get_config(void);
void update_config(void);
void get_and_send_data(void);
void start_sensors(void);
void stop_sensors(void);
void wait_for_ack(void);
void weather_station_run(void);
void weather_station_add_sensor(struct sensor * new_sensor);
void weather_station_init(int8_t  (*read_function)(void), void (*write_function)(int8_t ), void (*set_int_period_function)(uint32_t));

#endif

