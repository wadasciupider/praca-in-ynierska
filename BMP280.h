#include "efm32lg990f256.h"
#include "weather_station.h"
#ifndef BMP_h
#define BMP_h


void BMP_start(void);
void BMP_stop(void);
void BMP_init(void);
void BMP_deinit(void);
uint32_t get_pressure (void);

#endif
