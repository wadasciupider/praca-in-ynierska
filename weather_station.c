#include "weather_station.h"

struct sensor* SENSORS[MAXIMUM_NUMBER_OF_SENSORS];
uint32_t sensor_configuration;
uint8_t number_of_sensors=0;
int8_t  (*read_byte)(void) ;
void (*write_byte)(int8_t );
void (*change_interrupt_period)(uint32_t);

void weather_station_init(int8_t  (*read_function)(void), void (*write_function)(int8_t ), void (*set_int_period_function)(uint32_t)){
	read_byte=read_function;
	write_byte=write_function;
	change_interrupt_period=set_int_period_function;
}

void wait_for_ack(void){
	int8_t  ack =0;
	while (ack!=100){
		ack=read_byte();
	}
}
void weather_station_add_sensor(struct sensor * new_sensor){
	SENSORS[number_of_sensors]= new_sensor;
	number_of_sensors++;
}
void weather_station_run(void){
	get_config();
	update_config();
	start_sensors();
	get_and_send_data();
	stop_sensors();
	wait_for_ack();
}
void get_config(void){
	
	sensor_configuration = 0;
	write_byte(START_TRANSMISSION_BYTE);	
	wait_for_ack();
	sensor_configuration |= read_byte();   // Limitation to max 8 sensors
	sensor_configuration |= read_byte() << 8;
	sensor_configuration |= read_byte() << 16;
}

void update_config(void){

	for ( int i=0; i<number_of_sensors; i++){
			if(SENSORS[i]){
			if ((sensor_configuration & 1 << i) == 1 << i){
				if(SENSORS[i]->initialize){
					SENSORS[i]->initialize();
				}
			}
			else{
				if(SENSORS[i]->deinitialize){
					SENSORS[i]->deinitialize();
				}	
			}
		}
}
	change_interrupt_period((sensor_configuration >> 8)); 
}

void get_and_send_data(void){
	
	uint32_t tmp;
	for ( int i=0; i<number_of_sensors; i++){
		if(SENSORS[i] && (SENSORS[i]->get_data)){
			if ((sensor_configuration & 1 << i) == 1 << i){
				tmp = SENSORS[i]->get_data();
				for ( int j=3; j>=0; j--){
					write_byte ((tmp >> 8*j) & 0xFF);
				}
			}
		}
		else{
			if ((sensor_configuration & 1 << i) == 1 << i){
				for ( int j=3; j>=0; j--){
					write_byte (0);
				}
			}
		}
	}
}

void start_sensors(void){
	for ( int i=0; i<number_of_sensors; i++){
		if(SENSORS[i] && (SENSORS[i]->start)){
			if ((sensor_configuration & 1 << i) == 1 << i){
				SENSORS[i]->start();
			}
		}
	}
}

void stop_sensors(void){
	for ( int i=0; i<number_of_sensors; i++){
		if(SENSORS[i] && (SENSORS[i]->stop)){
			if ((sensor_configuration & 1 << i) == 1 << i){
				SENSORS[i]->stop();
			}
		}
	}
}

