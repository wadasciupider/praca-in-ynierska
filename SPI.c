#include "efm32lg990f256.h"
#include "SPI.h"

#define SPI_LOCATION 3 // gpioPortD (USART1 location #1)
#define SPI_CS 3   	 // PD3
#define SPI_SCLK 2   // PD2
#define SPI_MISO 1   // PD1
#define SPI_MOSI 0   // PD0
#define CLOCK_DIVIDER 14


void SPI_init (void){
  
	CMU->CTRL |= (1 << CLOCK_DIVIDER);                              // Set system clock to 24MHz
  CMU->OSCENCMD |= CMU_OSCENCMD_HFXOEN;                                
  while(! (CMU->STATUS & CMU_STATUS_HFXORDY) );                       
  CMU->CMD = CMU_CMD_HFCLKSEL_HFXO; 
  CMU->HFPERCLKEN0 |= CMU_HFPERCLKEN0_GPIO | CMU_HFPERCLKEN0_USART1;
	
	GPIO->P[SPI_LOCATION].MODEL |= (4 << 12) | (4 << 8) | (1 << 4) | (4 << 0);  // Configure CS, MOSI, SCLK as outputs, MISO as input
	GPIO->P[SPI_LOCATION].DOUTSET |= (1 << 4);	
   
  USART1->CTRL = UART_CTRL_SYNC | UART_CTRL_MVDIS | UART_CTRL_MSBF;           // Transmit MSB first, disable majority voting, use manual chip-select
  USART1->CLKDIV = (12 << 6);                               									// Divide clokc by 12, so that the baud rate is 1MB/s 
  USART1->CMD = UART_CMD_MASTEREN | UART_CMD_RXEN | UART_CMD_TXEN | UART_CMD_CLEARTX | UART_CMD_CLEARRX;; // Clear RX and TX buffers, enable master mode, enable transmitter/receiver
  USART1->IFC = _UART_IFC_MASK;                                     					// Clear USART1 interrupt flags
  USART1->ROUTE = UART_ROUTE_LOCATION_LOC1 | UART_ROUTE_RXPEN | UART_ROUTE_TXPEN| UART_ROUTE_CLKPEN; // Enable pins

}
	
void SPI_disable(void){
	USART1->ROUTE &= (~UART_ROUTE_RXPEN) & (~UART_ROUTE_TXPEN) & (~UART_ROUTE_CLKPEN);
	GPIO->P[SPI_LOCATION].MODEL &= 0xffff0000;
}
void SPI_enable(void){
	GPIO->P[SPI_LOCATION].MODEL |= (4 << 12) | (4 << 8) | (1 << 4) | (4 << 0);
	USART1->ROUTE = UART_ROUTE_LOCATION_LOC1 | UART_ROUTE_RXPEN | UART_ROUTE_TXPEN| UART_ROUTE_CLKPEN;
}
void CS_clr (void) {
  
	GPIO->P[SPI_LOCATION].DOUTCLR = (1 << SPI_CS);

}
 
void CS_set (void) {
  
	GPIO->P[SPI_LOCATION].DOUTSET = (1 << SPI_CS);

}
 
void SPI_putChar (uint8_t byte){
  
	while( !(USART1->STATUS & USART_STATUS_TXBL) ); 
  USART1->TXDATA = byte;                

}

uint8_t SPI_getChar (void) {
  
	while(! (USART1->STATUS & USART_STATUS_RXDATAV) ); 
  return(USART1->RXDATA);

}

uint16_t SPI_get2Char (void) {
		
	uint8_t tmp1, tmp2; 
	uint16_t out;
	SPI_putChar(0xFF);
	tmp1 = SPI_getChar();
	SPI_putChar(0xFF);
	tmp2 = SPI_getChar();
	out = 0;
	out |= tmp1 | (tmp2<<8);
	return out;

}
