#include "efm32lg990f256.h"
#include "I2C.h"
#include "MCP9808.h"

#define READ_ADDRESS ((0x18<<1)|1)
#define WRITE_ADDRESS (0x18<<1)
#define ALERT_MASK 0x1F
#define SIGN_MASK 0x10
#define STOP_MODE_MASK 0x01
#define DEFAULT_CONFIG 0x0

enum{
	CONFIG = 0x01,
	T_UPPER = 0x02,
	T_LOWER = 0x03,
	T_CRIT = 0x04,
	T_AMBIENT = 0x05,
	MANUFACTURER_ID = 0x06,
	DEVICE_ID = 0x07,
	RESOLUTION = 0x08
};

struct sensor MPC_sensor = {
	MCP_init,
	MCP_deinit,
	MCP_start,
	MCP_stop,
	get_temperature
};

void MCP_start (void){
	GPIO->P[2].MODEL |= (4);
	GPIO->P[2].DOUTSET = 1;
	for (int i =0;i<1000000;i++){}; 
	I2C_enable();
	I2C_start();

	I2C_putChar(WRITE_ADDRESS);  
	I2C_putChar(CONFIG);														
	I2C_putChar(DEFAULT_CONFIG);					// Set the mode of the sensor to START
	I2C_putChar(DEFAULT_CONFIG);
	I2C_stop();

}

void MCP_stop (void){
	I2C_disable();
	GPIO->P[2].DOUTCLR |= 1;
	GPIO->P[2].MODEL &= 0xfffffff0;
	// TODO: FIX STARTING, ENABLE THIS	
//	I2C_start();
//	I2C_putChar(WRITE_ADDRESS);  
//	I2C_putChar(CONFIG);														
//	I2C_putChar(STOP_MODE_MASK);					// Set the mode of the sensor to START
//	I2C_putChar(DEFAULT_CONFIG);
//	I2C_stop();

}

void MCP_init (void){
	CMU->HFPERCLKEN0 |= CMU_HFPERCLKEN0_GPIO;
GPIO->P[2].MODEL |= (4);
GPIO->P[2].DOUTSET |= 1 ;

	I2C_init();

}

void MCP_deinit(void){}				// TO DO

uint32_t get_temperature (void){
	
	uint8_t MSB,LSB;
	uint32_t temperature;
	
	I2C_start();
  I2C_putChar(WRITE_ADDRESS);  
	I2C_putChar(T_AMBIENT);														// Set the register pointer of the sensor to the T_AMBIENT register
	I2C_stop();
	
	I2C_start();
	I2C_putChar(READ_ADDRESS);												// Read temperature
	
	MSB = I2C_getChar(true);													// Get char, send ack
	LSB = I2C_getChar(false);													// Get char, don't send ack
	I2C_stop();
	MSB &= ALERT_MASK;							 	 								// Clear bits responsible for alerts
	
	if ((MSB & SIGN_MASK) == SIGN_MASK){ 
		MSB &= 0x0F; 																		// Clear the sign
		temperature = 25600 - (MSB*1600 + LSB*100/16);  // Calculate temperature
	}
	
	else{
		temperature = (MSB*16*100 + LSB*100/16);
	}
	return temperature;

}
