#include "efm32lg990f256.h"
#ifndef usart_h
#define usart_h

void usart_init (void);

void usart_writeChar (char c);
void usart_writeString(char* string);
char usart_readChar (void);

#endif
