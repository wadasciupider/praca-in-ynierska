#include "efm32lg990f256.h"
#include "led.h"

#define LED0 2
#define LED1 3
#define LED_PORT E

enum {
  A,
  B,
  C,
  D,
  E
};

void led_init(void){
	 
	CMU->HFPERCLKEN0 |= CMU_HFPERCLKEN0_GPIO;
	GPIO->P[LED_PORT].MODEL |= (4 << (4*LED1)) | (4 << (4*LED0));		// Enable led pins (PE2, PE3) as output
 
}
 
void led0_on(void){
	 
	//GPIO->P[LED_PORT].DOUTTGL = 1 << LED0;  
 
}

void led0_off(void){
	 
	GPIO->P[LED_PORT].DOUTCLR = 1 << LED0;  
 
}
 
void led1_on(void){
		
	GPIO->P[LED_PORT].DOUTSET = 1 << LED1;  
 
}

void led1_off(void){
	
	GPIO->P[LED_PORT].DOUTCLR = 1 << LED1;  
 
}

