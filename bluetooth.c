#include "efm32lg990f256.h"
#include "bluetooth.h"
#include "LEUART.h"

void waitForBluetoothPaired(void) {
	bool FLAG;
	while (1){
		while(! (GPIO->P[4].DIN & (1)) ) {};
			FLAG=1;
			for ( int i=0; i<100; i++){
				for ( int i=0; i<20000; i++){};
				if (!(GPIO->P[4].DIN & (1)) ){
					FLAG=0;
				}
			}
			if (FLAG == 1){
				break;
			}
	}

}

void initiBluetooth(void){
	leuart_init();
	CMU->HFPERCLKEN0 |= CMU_HFPERCLKEN0_GPIO;												// Enable clock for GPIO
	GPIO->P[4].MODEL |= (2) | (4<<4);
}
	
void turnBluetoothOn(void){
	LEUART_enable();
	GPIO->P[4].MODEL |= (2) | (4<<4);
	GPIO->P[4].DOUTSET |= 1 << 1;
	waitForBluetoothPaired();
}
void turnBluetoothOff(void){
GPIO->P[4].DOUTCLR |= 1 << 1;
	GPIO->P[4].MODEL &= 0xffffff00;
	LEUART_disable();  
}
