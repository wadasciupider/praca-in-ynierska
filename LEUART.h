#include "efm32lg990f256.h"
#include <stdbool.h>
#ifndef leuart_h
#define leuart_h

void leuart_init (void);

void leuart_writeChar (int8_t  c);
void leuart_writeString(int8_t * string);
int8_t  leuart_readChar (void);
void LEUART_disable (void);
void LEUART_enable (void);

#endif
