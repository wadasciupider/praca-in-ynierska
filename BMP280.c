#include "efm32lg990f256.h"
#include "SPI.h"
#include "BMP280.h"
#include "stdbool.h"
#include "USART.h"

#define DUMMY_DATA 0xFF
#define SINGLE_CONVERSION_MASK 0x02
#define PRESSURE_OVERSAMPLING_MASK 0x1C 
#define TEMPERATURE_OVERSAMPLING_MASK 0xE0


typedef long int BMP280_S32_t;
typedef unsigned long int BMP280_U32_t;

long int t_fine;						// Variable shared between pressure and temperature

struct sensor BMP_sensor={
	BMP_init,
	BMP_deinit,
	BMP_start,
	BMP_stop,
	get_pressure
};

enum{
	ID = 0x0D,
	RESET = 0xE0,
	STATUS = 0xF3,
	CTRL_MEAS_READ = 0xF4,
	CTRL_MEAS_WRITE = 0x74,
	CONFIG = 0xF5,
	PRESS_MSB = 0xF7,
	PRESS_LSB = 0xF8,
	PRESS_XLSB = 0xF9,
	TEMP_MSB = 0xFA,
	TEMP_LSB = 0xFB,
	TEMP_XLSB = 0xFC,
	CALIBRATION_REGISTER = 0x88
};

struct calibration_structure {
	unsigned short dig_T1;
	signed short dig_T2;
	signed short dig_T3;
	unsigned short dig_P1;
	signed short dig_P2;
	signed short dig_P3;
	signed short dig_P4;
	signed short dig_P5;
	signed short dig_P6;
	signed short dig_P7;
	signed short dig_P8;
	signed short dig_P9;
} calibration;

void BMP_start(void){
  SPI_enable();
}
void BMP_stop(void){
	SPI_disable();
}
	
void BMP_init(void){
	
	SPI_init();					 // Initialize SPI
	CS_clr();
	
	SPI_putChar(CALIBRATION_REGISTER);   // Read calibration data to global variables       
  SPI_getChar(); 
  
  calibration.dig_T1 |= SPI_get2Char();
	calibration.dig_T2 |= SPI_get2Char();
	calibration.dig_T3 |= SPI_get2Char();
	calibration.dig_P1 |= SPI_get2Char();
	calibration.dig_P2 |= SPI_get2Char();
	calibration.dig_P3 |= SPI_get2Char();
	calibration.dig_P4 |= SPI_get2Char();
	calibration.dig_P5 |= SPI_get2Char();
	calibration.dig_P6 |= SPI_get2Char();
	calibration.dig_P7 |= SPI_get2Char();
	calibration.dig_P8 |= SPI_get2Char();
	calibration.dig_P9 |= SPI_get2Char();

  CS_set();
	
}

void BMP_deinit(void){}				// TO DO

static BMP280_S32_t bmp280_compensate_T_int32(BMP280_S32_t adc_T){	// Temperature calculation function provided by producer,
		
	BMP280_S32_t var1, var2, T;																			// used to calculate t_fine used in pressure calculation
	var1=((((adc_T>>3)-((BMP280_S32_t)calibration.dig_T1<<1)))*((BMP280_S32_t)calibration.dig_T2))>>11;
	var2 = (((((adc_T>>4) - ((BMP280_S32_t)calibration.dig_T1)) * ((adc_T>>4) - ((BMP280_S32_t)calibration.dig_T1))) >> 12) * ((BMP280_S32_t)calibration.dig_T3)) >> 14;
	t_fine = var1 + var2;
	T = (t_fine * 5 + 128) >> 8;
	return T;
	
}

static BMP280_U32_t bmp280_compensate_P_int32(BMP280_S32_t adc_P){ // Pressure calculation function provided by producer,
		
	BMP280_S32_t var1, var2;
	BMP280_U32_t p;
	
	var1 = (((BMP280_S32_t)t_fine)>>1) - (BMP280_S32_t) 64000;
	var2 = (((var1>>2) * (var1>>2)) >> 11 ) * ((BMP280_S32_t)calibration.dig_P6);
	var2 = var2 + ((var1*((BMP280_S32_t)calibration.dig_P5))<<1);
	var2 = (var2>>2)+(((BMP280_S32_t)calibration.dig_P4)<<16);
	var1 = (((calibration.dig_P3 * (((var1>>2) * (var1>>2)) >> 13 )) >> 3) + (((( BMP280_S32_t)calibration.dig_P2) * var1)>>1))>>18;
	var1 =((((32768+var1))*((BMP280_S32_t)calibration.dig_P1))>>15);
	
	if (var1 == 0){
		return 0; // avoid exception caused by division by zero
	}
	
	p = (((BMP280_U32_t)(((BMP280_S32_t)1048576) -adc_P) -(var2>>12)))*3125;
	
	if (p < 0x80000000){
		p = (p << 1) / ((BMP280_U32_t)var1);
	}
	
	else{
		p = (p / (BMP280_U32_t)var1) * 2;
	}
	
	var1 = (((BMP280_S32_t)calibration.dig_P9) * ((BMP280_S32_t)(((p>>3) * (p>>3))>>13)))>>12;
	var2 = (((BMP280_S32_t)(p>>2)) * ((BMP280_S32_t)calibration.dig_P8))>>13;
	p = (BMP280_U32_t)((BMP280_S32_t)p + ((var1 + var2 + calibration.dig_P7) >> 4));
	return p;
	
}

static bool get_status (void){
	
	uint8_t status;
	
	SPI_putChar(STATUS);         
  status = SPI_getChar();
	SPI_putChar(DUMMY_DATA);										          
  status = SPI_getChar();
	return status == 0;
	
}

uint32_t get_pressure (void){
	
	uint8_t tmp1, tmp2, tmp3;
  uint32_t  adc_t=0, adc_p=0;
  CS_clr();   
   
  SPI_putChar(CTRL_MEAS_WRITE);         // Wake sensor up and start a conversion
  tmp1 = SPI_getChar();
	SPI_putChar(SINGLE_CONVERSION_MASK|PRESSURE_OVERSAMPLING_MASK|TEMPERATURE_OVERSAMPLING_MASK);										// Trigger single conversion, set oversampling rates to the highest         
  tmp1 = SPI_getChar();
	  
	while(!get_status());									// Wait till conversion is complete	
//	TODO REMOVE THE WAIT
	for (int i=0;i<10000;i++){}
	SPI_putChar(PRESS_MSB);          			// Read pressure and temperature data
  tmp1 = SPI_getChar(); 
  
  SPI_putChar(DUMMY_DATA);
  tmp1 = SPI_getChar();
	SPI_putChar(DUMMY_DATA);
  tmp2 = SPI_getChar();
	SPI_putChar(DUMMY_DATA);
  tmp3 = SPI_getChar();
  adc_p |= tmp3|(tmp2<<4)|(tmp1<<12);
	SPI_putChar(DUMMY_DATA);
  tmp1 = SPI_getChar();
	SPI_putChar(DUMMY_DATA);
  tmp2 = SPI_getChar();
	SPI_putChar(DUMMY_DATA);
  tmp3 = SPI_getChar();
  adc_t |= tmp3|(tmp2<<4)|(tmp1<<12);
  CS_set();
	bmp280_compensate_T_int32(adc_t);
	return bmp280_compensate_P_int32(adc_p); // Calculate pressure

}
