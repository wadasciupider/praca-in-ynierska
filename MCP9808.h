#include "efm32lg990f256.h"
#include "weather_station.h"
#ifndef mcp9808_h
#define mcp9808_h

uint32_t get_temperature (void);
void MCP_init (void);
void MCP_deinit(void);
void MCP_start (void);
void MCP_stop (void);

#endif
