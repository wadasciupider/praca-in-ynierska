#include "efm32lg990f256.h"
#include "LEUART.h"
#include <string.h>

#define LEUART_PORT 3 //Use location 0
#define LEUART_TX 4   //PTD4
#define LEUART_RX 5   //PTD5
#define LEUART_CLOCK_DIVIDER 616


void leuart_init (void){
	
  /* Enable clock */
  CMU->OSCENCMD |= CMU_OSCENCMD_LFRCOEN;
	CMU->LFCLKSEL |= CMU_LFCLKSEL_LFB_LFRCO;
	CMU->LFBPRESC0 |= CMU_LFBPRESC0_LEUART0_DIV1;
  CMU->LFBCLKEN0 |= CMU_LFBCLKEN0_LEUART0;
	CMU->HFCORECLKEN0 |= CMU_HFCORECLKEN0_LE;
  CMU->HFPERCLKEN0 |= CMU_HFPERCLKEN0_GPIO;
  
	while (LEUART0->SYNCBUSY & LEUART_SYNCBUSY_CMD);
  LEUART0->CMD = LEUART_CMD_RXDIS | LEUART_CMD_TXDIS;

  LEUART0->FREEZE = 1;
	// Baud rate 9600
	while (LEUART0->SYNCBUSY & LEUART_SYNCBUSY_CLKDIV);
  LEUART0->CLKDIV = LEUART_CLOCK_DIVIDER;
	//Enable
  LEUART0->CMD = LEUART_CMD_RXEN | LEUART_CMD_TXEN;

	LEUART0->FREEZE = 0;

	GPIO->P[LEUART_PORT].MODEL |= (2 << (4*LEUART_RX))|(4 << (4*LEUART_TX));  						// Set TX as output, RX as input
	GPIO->P[LEUART_PORT].DOUTSET |= (1 << LEUART_TX);  																	// Initialize TX high
	
  /* Enable signals TX, RX at location 0 */
  LEUART0->ROUTE = LEUART_ROUTE_RXPEN | LEUART_ROUTE_TXPEN;
	while (LEUART0->SYNCBUSY & LEUART_SYNCBUSY_CMD);
	LEUART0->IFC = _LEUART_IFC_MASK;
} 
void LEUART_disable(void){
	GPIO->P[LEUART_PORT].DOUTCLR |= (1 << LEUART_TX); 
	GPIO->P[LEUART_PORT].MODEL &= 0xff00ffff;
}
void LEUART_enable(void){
	GPIO->P[LEUART_PORT].MODEL |= (2 << (4*LEUART_RX))|(4 << (4*LEUART_TX));  						// Set TX as output, RX as input
	GPIO->P[LEUART_PORT].DOUTSET |= (1 << LEUART_TX);  																	// Initialize TX high
}
void leuart_writeChar (int8_t  c){
	
	while( !(LEUART0->STATUS & (LEUART_STATUS_TXBL)) ); // wait for TX buffer to empty
	while (LEUART0->SYNCBUSY & LEUART_SYNCBUSY_TXDATA);
	LEUART0->TXDATA = c;
}
void leuart_writeString (int8_t * string){
	
	for(int i=0; i<strlen(string); i++){
			leuart_writeChar(string[i]);
	}

}
int8_t  leuart_readChar (void){

	while( !(LEUART0->STATUS & (LEUART_STATUS_RXDATAV)) ); 
	return LEUART0->RXDATA; 

}
