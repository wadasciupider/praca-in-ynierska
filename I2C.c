#include "efm32lg990f256.h"
#include "I2C.h"

#define I2C_PORT 2			// gpioPortC
#define I2C_SDA 4				// PC4
#define I2C_SCLK 5			// PC5
#define CLOCK_DIVIDER 14
#define I2C_CLOCK_DIVIDER 29

void I2C_init (void) {
	
	CMU->CTRL |= (1 << CLOCK_DIVIDER);                              									// Set system clock to 24MHz
  CMU->OSCENCMD |= CMU_OSCENCMD_HFXOEN;                                
  while(! (CMU->STATUS & CMU_STATUS_HFXORDY) );                       
  CMU->CMD = CMU_CMD_HFCLKSEL_HFXO;
 	CMU->HFPERCLKEN0 &= ~CMU_HFPERCLKEN0_I2C0;
  CMU->HFPERCLKEN0 |= CMU_HFPERCLKEN0_GPIO | CMU_HFPERCLKEN0_I2C1;
	
	GPIO->P[I2C_PORT].MODEL |= (11 << (4*I2C_SCLK)) | (11 << (4*I2C_SDA));						// Set mode of the I2C pins to open drain with pull-up
  GPIO->P[I2C_PORT].DOUTSET |= (1 << I2C_SCLK) | (1 << I2C_SDA); 										// Initialize SDA, SCL high
	I2C1->CTRL = I2C_CTRL_AUTOACK  | I2C_CTRL_EN ;           													// Enable I2C mode, automatically send ACK
  I2C1->CMD = I2C_CMD_CLEARPC | I2C_CMD_CLEARTX | I2C_CMD_ABORT ; 									// Clear TX, pending commands and send abort
  I2C1->CLKDIV = I2C_CLOCK_DIVIDER;                          																			// Divide clock to achive 100kHz transmission speed
  I2C1->ROUTE = I2C_ROUTE_SDAPEN|I2C_ROUTE_SCLPEN;                    							// Enable SCL and SDA pins, select I2C LOCATION 0

}
void I2C_disable(void){
	GPIO->P[I2C_PORT].DOUTCLR |= (1 << I2C_SCLK) | (1 << I2C_SDA); 
	GPIO->P[I2C_PORT].MODEL &= 0xff00ffff;
}
void I2C_enable(void){
	GPIO->P[I2C_PORT].MODEL |= (11 << (4*I2C_SCLK)) | (11 << (4*I2C_SDA));						// Set mode of the I2C pins to open drain with pull-up
  GPIO->P[I2C_PORT].DOUTSET |= (1 << I2C_SCLK) | (1 << I2C_SDA); 										// Initialize SDA, SCL high
}
void I2C_putChar (uint8_t byte) {
  
	while(!(I2C1->STATUS & I2C_STATUS_TXBL));   // wait for TX buffer to empty
  I2C1->TXDATA = byte;         								// write control byte to TX buffer
  while(!(I2C1->STATUS & I2C_STATUS_TXBL));   // wait for TX buffer to empty         

}

uint8_t I2C_getChar (bool ack) {
	
	char byte;
	while(!(I2C1->STATUS & I2C_STATUS_RXDATAV));    // wait for data to arrive
  byte = I2C1->RXDATA;            								// read rx data and store in 'buffer'
	return byte;

}

void I2C_start (void) {
  
	while(I2C1->STATE & I2C_STATE_BUSY);           // wait for bus to become idle
  I2C1->CMD |= I2C_CMD_START;                    // send START command          

}

void I2C_stop (void) {
	
	I2C1->CMD |= I2C_CMD_STOP;                     // send STOP command   

}
