#include "efm32lg990f256.h"
#ifndef led_h
#define led_h

void led_init(void); 

void led0_on(void);
void led0_off(void); 
void led1_on(void);
void led1_off(void);

#endif
