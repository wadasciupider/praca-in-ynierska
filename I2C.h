#include "efm32lg990f256.h"
#include "stdbool.h"

#ifndef i2c_h
#define i2c_h

void I2C_init (void);

void I2C_putChar (uint8_t byte);
uint8_t I2C_getChar (bool ack);
void I2C_start (void);
void I2C_stop (void);
void I2C_disable(void);
void I2C_enable(void);

#endif

