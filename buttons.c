#include "efm32lg990f256.h"
#include "buttons.h"

#define BUTTON0 9
#define BUTTON1 10
#define BUTTON_PORT B

enum {
  A,
  B,
  C,
  D,
  E
};

void button_init(void){
	
	GPIO->P[BUTTON_PORT].MODEH = (2 << 4) | (2 << 8);   						// Enable button pins (PB9, PB10) as input
	GPIO->P[BUTTON_PORT].DOUT = (1 << BUTTON1) | (1 << BUTTON0);		// Enable button pins pull-up
	CMU->HFPERCLKEN0 |= CMU_HFPERCLKEN0_GPIO;												// Enable clock for GPIO
 
}
 
bool button0_read(void){
	
	return (!(GPIO->P[BUTTON_PORT].DIN & (1 << BUTTON0)));
 
}

bool button1_read(void){
	
	return (!(GPIO->P[BUTTON_PORT].DIN & (1 << BUTTON1))); 
 
}
