#include "efm32lg990f256.h"
#ifndef rtc_h
#define rtc_h

void RTC_init(void);
void RTC_set_period(uint32_t periodInSeconds);
void RTC_start(void);
void RTC_stop(void);
void RTC_clear_interrupts(void);
#endif
